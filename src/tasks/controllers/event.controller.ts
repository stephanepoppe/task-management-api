import { Controller, Get, Query, Param, UseInterceptors } from '@nestjs/common';
import { EventService } from '../services/event.service';
import { EventFilter } from '../dto/filter-event.dto';
import { EventResponse } from '../dto/event-response.dto';
import { RavenInterceptor } from 'nest-raven';

@Controller('events')
export class EventController {
    constructor(
        private eventService: EventService
    ) {}

    @UseInterceptors(new RavenInterceptor())
    @Get()
    getAllEvents(@Query() filter: EventFilter, @Query('offset') offset:number, @Query('limit') limit:number): Promise<{"data": EventResponse[], "count": number}> {
        return this.eventService.getEvents(filter, limit, offset);
    }
}   