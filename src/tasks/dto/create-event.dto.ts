export class CreateEventDto {
    completedAt: Date;
    taskId: string;
    userId: string;
}