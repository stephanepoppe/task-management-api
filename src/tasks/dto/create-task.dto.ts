import { IsNotEmpty, isNotEmpty, IsEnum } from 'class-validator'
import { Frequency } from '../enums/task-frequency.enum';
import { Day } from '../enums/task-days.enum';

export class CreateTaskDto {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    location: string;

    @IsNotEmpty()
    @IsEnum(Frequency)
    frequency: string;

    // @IsNotEmpty()
    // @IsEnum(Day)
    day:string

    @IsNotEmpty()
    categoryId: string;
}