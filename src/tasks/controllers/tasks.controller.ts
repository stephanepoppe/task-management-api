import { Controller, Get, Post, Body, Param, Delete, Patch, UsePipes, ValidationPipe, Query } from '@nestjs/common';
import { TasksService } from '../services/tasks.service';
import { CreateTaskDto } from '../dto/create-task.dto';
import { UpdateTaskDto } from '../dto/update-task.dto';
import { Task } from '../entities/task.entity';
import { FilterDto } from '../dto/filter-task.dto';
import { CreateEventDto } from '../dto/create-event.dto';
import { Event } from '../entities/event.entity';
import { EventService } from '../services/event.service';
import { TaskResponse } from '../dto/task-response.dto';
import { UseInterceptors } from '@nestjs/common/decorators/core/use-interceptors.decorator';
import { RavenInterceptor } from 'nest-raven';

@Controller('tasks')
export class TasksController {
    constructor(
        private tasksService: TasksService,
        private eventService: EventService
    ) {}

    @UseInterceptors(new RavenInterceptor())
    @Get('')
    getAllTasks(
        @Query() filterDto: FilterDto, 
        @Query('limit') limit:number, 
        @Query('offset') offset:number 
    ): Promise<{"data": TaskResponse[], 'count': number}> {
        return this.tasksService.getAllTasks(filterDto, limit, offset);
    }

    @UseInterceptors(new RavenInterceptor())
    @Get('/:id')
    getTaskById(@Param('id') id: string): Promise<Task> {
        return this.tasksService.getTaskById(id);
    }

    @UseInterceptors(new RavenInterceptor())
    @Delete('/:id')
    removeTaskById(@Param('id') id: string): void {
        this.tasksService.removeById(id);
    }

    @UseInterceptors(new RavenInterceptor())
    @Post(':id/events')
    createEventForTask(@Param('id') id: string, @Body() createEventDto: CreateEventDto): Promise<Event> {
        return this.eventService.createEvent(id, createEventDto);
    }

    @UseInterceptors(new RavenInterceptor())
    @Post()
    @UsePipes(ValidationPipe) 
    createTask(@Body() createTaskDto: CreateTaskDto): Promise<Task> {
        return this.tasksService.createTask(createTaskDto);
    }

    @UseInterceptors(new RavenInterceptor())
    @Patch('/:id/update')
    @UsePipes(ValidationPipe)
    updateTask(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto): Promise<Task> {
        return this.tasksService.updateTask(id, updateTaskDto);
    }
}   
