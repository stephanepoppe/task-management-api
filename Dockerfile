FROM node:19-alpine3.15

RUN apk update && apk add bash

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./
COPY tsconfig.json ./

COPY . .

RUN npm install ci
RUN npm run build   

USER node

EXPOSE 80:3000
CMD ["npm", "run", "start"]