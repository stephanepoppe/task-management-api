import { Repository } from "typeorm";
import { Event } from '../entities/event.entity';

export class EventRepository extends Repository<Event> {}