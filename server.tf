terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

# Create a dokku droplet
resource "digitalocean_droplet" "dokku_1" {
  image = var.droplet_image
  name = "dokku-1"
  region = var.region
  size = var.droplet_size
  ssh_keys = ["f2:94:c3:bb:96:98:41:c9:62:e3:32:fa:7a:27:77:7b"]  
  
  provisioner "remote-exec" {
    inline = [
        "wget https://raw.githubusercontent.com/dokku/dokku/master/bootstrap.sh;",
        "sudo DOKKU_TAG=v0.21.1 bash bootstrap.sh",
        "dokku apps:create todo-api-staging",
        "sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git", 
        "sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git"       
    ]
  }

  connection {
    type     = "ssh"
    user     = "root"
    host = digitalocean_droplet.dokku_1.ipv4_address
    private_key = file(var.pvt_key)
  }
}

output "public_ip_server" {
  value = digitalocean_droplet.dokku_1.ipv4_address
}