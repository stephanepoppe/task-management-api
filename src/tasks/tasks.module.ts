import { Module } from '@nestjs/common';
import { TasksController } from './controllers/tasks.controller';
import { TasksService } from './services/tasks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventRepository } from './repository/event.repository';
import { UserRepository } from './repository/user.repository';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { EventService } from './services/event.service';
import { HealthController } from './controllers/health.controller';
import { EventController } from './controllers/event.controller';
import { CategoryController } from './controllers/category.controller';
import { CategoryRepository } from './repository/category.repository';
import { CategoryService } from './services/category.service';
import { databaseProviders } from './providers/database-providers';
import { repositoryProviders } from './providers/taskProviders';

@Module({
  imports: [
    TypeOrmModule.forFeature([EventRepository, UserRepository, CategoryRepository]),
  ],
  controllers: [TasksController, UserController, HealthController, EventController, CategoryController],
  providers: [TasksService, EventService, UserService, CategoryService, ...databaseProviders, ...repositoryProviders],
})
export class TasksModule {}
