import { BaseEntity, Column, PrimaryColumn, ManyToOne, Entity, OneToMany } from 'typeorm';
import { Event } from './event.entity';

@Entity()
export class User extends BaseEntity {
    @PrimaryColumn()
    id: string;

    @Column()
    name:string;

    @Column()
    emoji: string

    @OneToMany(type => Event, event => event.user)
    task: Event;
}