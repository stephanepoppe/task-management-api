import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as Sentry from "@sentry/node";
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  Sentry.init({ 
    dsn: "https://26b10b04b60743c6b05889f26465e71d@o468653.ingest.sentry.io/5496797",
    environment: configService.get('ENVIRONMENT')
  });
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
