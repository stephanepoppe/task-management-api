import { CategoryResponse } from './category-response.dto';
export class TaskResponse {
    id: string;
    title: string;
    location: string;
    frequency: string;
    weekday: string;
    // event: any;
    completed: boolean;
    category: CategoryResponse;
}