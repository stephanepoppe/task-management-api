import { User } from "../entities/user.entity";
import { Task } from '../entities/task.entity';

export class EventResponse {
    id: string;
    task: Task;
    completedAt: Date;
    userId: string;
    user: User;
}