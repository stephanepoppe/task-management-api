import { Controller, Get, Post, Body, Param, Delete, Patch, UsePipes, ValidationPipe, Query, UseInterceptors } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { UserService } from '../services/user.service';
import { createUserDto } from '../dto/create-user.dto';
import { RavenInterceptor } from 'nest-raven';

@Controller('users')
export class UserController {
    constructor(private userService: UserService) {}

    @UseInterceptors(new RavenInterceptor())
    @Get()
    getAllTasks(): Promise<User[]> {
        return this.userService.getAllUsers();
    }

    @UseInterceptors(new RavenInterceptor())
    @Post('')
    createUser(@Body() createUserDto: createUserDto): Promise<User> {
        return this.userService.createUser(createUserDto);
    }

    @UseInterceptors(new RavenInterceptor())
    @Patch('/:id')
    updateUser(@Param('id') id: string, @Body() createUserDto: createUserDto): Promise<User> {
        return this.userService.updateUser(id, createUserDto);
    }
}   
