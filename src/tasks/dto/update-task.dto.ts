import { IsNotEmpty, isNotEmpty, IsEnum } from 'class-validator'
import { Frequency } from '../enums/task-frequency.enum';
import { Day } from '../enums/task-days.enum';

export class UpdateTaskDto {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    location: string;

    @IsNotEmpty()
    @IsEnum(Frequency)
    frequency: string;

    // @IsNotEmpty()
    // @IsEnum(Day)
    day:string

    category: string
}