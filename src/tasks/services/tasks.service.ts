import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from '../dto/create-task.dto';
import { UpdateTaskDto } from '../dto/update-task.dto';
import { Task } from '../entities/task.entity';
import { FilterDto } from '../dto/filter-task.dto';
import * as moment from 'moment';
import { TaskResponse } from '../dto/task-response.dto';
import { Event } from '../entities/event.entity';
import { Frequency } from '../enums/task-frequency.enum';
import { CategoryService } from './category.service';
import { EventService } from './event.service';

@Injectable()
export class TasksService {
    constructor(
        private categoryService: CategoryService,
        private eventService: EventService
    ){}

    async getAllTasks(filterDto: FilterDto, limit:number = 100, offset: number = 0): Promise<{"data": TaskResponse[], 'count': number}> {
        const tasksAndCount = await Task.getTasksByDay(filterDto, limit, offset);
        const tasks = tasksAndCount[0];
        const taskResponses = await this.enrichTasks(tasks, filterDto);
        return {"data": taskResponses, 'count': tasksAndCount[1]}
    }

    async enrichTasks(tasks:Task[], filterDto:FilterDto) {
        return Promise.all(tasks.map(async task => {
            const endDate = moment(filterDto.date).hour(23).minute(59);
            const startDate = moment(endDate).hour(0).minute(0);
            if (task.frequency === Frequency.Monthly) {
                startDate.subtract(1, 'months');
            } else if (task.frequency === Frequency.Weekly) {
                startDate.subtract(6, 'days');
            }
            const event:Event = await this.eventService.getEventByTaskAndPeriod(task, startDate, endDate)
            let isCompleted = false;
            if (event) {
                isCompleted = true;
            }
            return {
                id: task.id,
                title: task.title,
                location: task.location,
                frequency: task.frequency,
                weekday: task.weekday,
                event: event,
                completed: isCompleted,
                category: {
                    id: task.category.id, 
                    title: task.category.title
                }
            };
        }));  
    }

    async getTaskById(id: string): Promise<Task> {
        const task = await Task.findOne({
            where: {id: id},
            relations: ['category']
        });

        if (!task) {
            throw new NotFoundException(`Task with ID "${id}" not found`);
        }
        return task;
    }

    async removeById(id: string): Promise<void> { 
        const result = await Task.delete(id);
        
        if (result.affected === 0) {
            throw new NotFoundException(`Task with ID "${id}" could not be deleted`);
        }
    }

    async updateTask(id: string, updateTaskDto: UpdateTaskDto): Promise<Task>{
        let task = await this.getTaskById(id);
        if (task !== null) {
            Task.updateTask(task, updateTaskDto);
            
        }
        return task;
    }

    async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
        const category = await this.categoryService.getCategoryById(createTaskDto.categoryId);
        return Task.createTask(createTaskDto, category);
    }
}
