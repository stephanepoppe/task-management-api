import { Injectable, NotFoundException, Inject } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { createUserDto } from '../dto/create-user.dto';
import { v1 as uuid } from 'uuid';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor(
        @Inject('USER_REPOSITORY')
        private userRepository: Repository<User>,
    ){}

    async getAllUsers(): Promise<User[]> {
        return this.userRepository.find({order: {
            name: "ASC",
        }});
    }

    async getUserById(id: string): Promise<User> {
        const user = await this.userRepository.findOneBy({id});
        if (!user) {
            throw new NotFoundException(`User with ID "${id}" not found`);
        }
        return user;
    }

    async createUser(createUserDto: createUserDto): Promise<User> {
        const {name, emoji} = createUserDto;
        const user = new User();
        user.id = uuid();
        user.name = name;
        user.emoji = "😆";
        await user.save();
        return user;
    }

    async updateUser(id: string , createUserDto: createUserDto): Promise<User> {
        const {name, emoji} = createUserDto;
        const user = await this.getUserById(id);
        if (name !== '') {
            user.name = name;
        }
        if (emoji !== '') {
            user.emoji = emoji
        }
        await user.save();
        return user;
    }
}
