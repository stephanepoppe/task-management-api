import { BaseEntity, Column, PrimaryColumn, ManyToOne, Entity } from 'typeorm';
import { CreateEventDto } from '../dto/create-event.dto';
import { Task } from './task.entity';
import { User } from './user.entity';
import { v1 as uuid } from 'uuid';
import { EventFilter } from '../dto/filter-event.dto';

@Entity()
export class Event extends BaseEntity {
    @PrimaryColumn()
    id: string;

    @Column()
    completedAt: Date

    @ManyToOne(type => Task, task => task.events)
    task: Task;

    @ManyToOne(type => User, user => user)
    user: User;

    @Column()
    taskId: string;

    @Column({nullable: true})
    category: string;

    @Column({nullable: true})
    userName: string;

    static async createEvent(task: Task, user: User, createEventDto: CreateEventDto): Promise<Event> {
        const { completedAt, taskId } = createEventDto;
        const event = new Event();
        event.id = uuid();
        event.completedAt = completedAt;
        event.task = task;
        event.user = user;
        await event.save();
        return event;
    }

    static async getEventByTaskAndPeriod(task:Task, startDate, endDate): Promise<Event> {
        const qb = this.createQueryBuilder("event");
        qb.where("event.task = :id", { id: task.id })
            .leftJoinAndSelect("event.user", "user")
            .andWhere("event.completedAt BETWEEN :startDate AND :endDate" , {startDate: startDate.format('YYYY-MM-DD HH:mm'), endDate: endDate.format('YYYY-MM-DD HH:mm')})
            .orderBy("event.completedAt", "DESC")
            .limit(1);
        return qb.getOne();
    }

    static async getEvents(filter:EventFilter, limit:number = 10, offset:number = 0): Promise<[Event[], number]> {
        const qb = this.createQueryBuilder("event");
        if (filter.taskId) {
            qb.where("event.task = :id", { id: filter.taskId })
        }
        qb.leftJoinAndSelect("event.task", "task")
            .leftJoinAndSelect("event.user", "user")
            .leftJoinAndSelect("task.category", "category")
            .orderBy('event.completedAt', 'DESC')
            .offset(offset)
            .limit(limit);
        return qb.getManyAndCount();
    }
}