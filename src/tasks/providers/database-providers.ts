import { DataSource } from "typeorm";
import { ConfigService } from '@nestjs/config';

export const databaseProviders = [
    {
      provide: 'DATA_SOURCE',
      useFactory: async (configService: ConfigService) => {
        const dataSource = new DataSource({
          type: 'postgres',
          host: configService.get('DB_HOST'),
          port: 5432,
          url: configService.get('DATABASE_URL'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASSWORD'),
          database: configService.get('DB_DATABASE'),
          entities: [
              __dirname + '/../entities/*.entity{.ts,.js}',
          ],
          synchronize: true,
        });
  
        return dataSource.initialize();
      },
      inject: [ConfigService]
    }]
