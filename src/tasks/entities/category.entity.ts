import { BaseEntity, Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { CreateCategory } from '../dto/create-category.dto';
import { Task } from './task.entity';
import { v1 as uuid } from 'uuid';

@Entity()
export class Category extends BaseEntity {
    @PrimaryColumn()
    id: string;

    @Column()
    title: string;

    @OneToMany(type => Task, task => task.category)
    tasks: Task[];

    static async createCategory(createCategory: CreateCategory): Promise<Category> {
        const category = new Category();
        category.id = uuid();
        category.title = createCategory.title;
        await category.save();
        return category;
    }
}