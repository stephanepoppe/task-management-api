import { Injectable, Inject } from "@nestjs/common";
import { EventRepository } from '../repository/event.repository';
import { CreateEventDto } from '../dto/create-event.dto';
import { Event } from '../entities/event.entity';
import { UserService } from './user.service';
import { EventFilter } from '../dto/filter-event.dto';
import { EventResponse } from '../dto/event-response.dto';
import { Task } from "../entities/task.entity";

@Injectable()
export class EventService {
    constructor(
        @Inject('EVENT_REPOSITORY')
        private eventRepository: EventRepository,
        private userService: UserService,
    ){}

    async createEvent(taskId: string, createEventDto: CreateEventDto): Promise<Event> {
        const task = await Task.findOneBy({id: taskId});
        const {userId} = createEventDto;
        
        const user = await this.userService.getUserById(userId);
        return await Event.createEvent(task, user, createEventDto);
    }

    async getEvents(eventFilter: EventFilter, limit:number, offset:number): Promise<{"data" : EventResponse[], "count" : number}>  {
        const events:[Event[], number] = await Event.getEvents(eventFilter, limit, offset);
        const count:number = events[1];
        
        const eventsResponse: EventResponse[] = ( events[0]).map( event => {
            return {
                id: event.id,
                task: event.task,
                completedAt: event.completedAt,
                userId: event.user.id,
                user: event.user
            };
        });
        
        return {"data": eventsResponse, 'count': count}
    }

    async getEventByTaskAndPeriod(task:Task, startDate, endDate): Promise<Event> {
        return Event.getEventByTaskAndPeriod(task, startDate, endDate);
    }
}