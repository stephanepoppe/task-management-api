

## Description

Task management REST API

## Installation

```bash
$ npm install
```

## Running the app with docker-compose

```bash
$ docker-compose up -d
```

## Available endpoints

- GET / -> health check
- GET /tasks
- GET /task/{id}
- DELETE /task/{id}
- PATCH /task/{id}
- POST /task
- GET /category

## Example
```
curl -X "POST" "http://localhost/tasks" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "title": "On localhost with docker",
  "description": "for real"
}
```

