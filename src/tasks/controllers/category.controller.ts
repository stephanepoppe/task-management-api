import { Controller, Get, Query, Param, Post, Body, UseInterceptors } from '@nestjs/common';
import { RavenInterceptor } from 'nest-raven';
import { Category } from '../entities/category.entity';
import { CategoryService } from '../services/category.service';
import { CreateCategory } from '../dto/create-category.dto';

@Controller('categories')
export class CategoryController {
    constructor(
        private categoryService: CategoryService
    ) {}

    @UseInterceptors(new RavenInterceptor())
    @Post()
    createCategory(@Body() createCategory:CreateCategory): Promise<Category> {
        return this.categoryService.createCategory(createCategory);
    }

    @UseInterceptors(new RavenInterceptor())
    @Get()
    getAllCategories(): Promise<Category[]> {
        return this.categoryService.getCategories();
    }
}   