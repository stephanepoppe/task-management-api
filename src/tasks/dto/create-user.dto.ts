import { IsNotEmpty, isNotEmpty } from 'class-validator'

export class createUserDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    emoji: string;
}