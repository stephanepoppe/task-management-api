import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, PrimaryColumn, OneToMany, ManyToOne } from 'typeorm';
import { Event } from './event.entity';
import { Category } from './category.entity';
import { FilterDto } from '../dto/filter-task.dto';
import moment = require('moment');
import { CreateTaskDto } from '../dto/create-task.dto';
import { UpdateTaskDto } from '../dto/update-task.dto';
import { v1 as uuid } from 'uuid';

@Entity()
export class Task extends BaseEntity {
    
    @PrimaryColumn()
    id: string;

    @Column()
    title: string;
    
    @Column()
    location: string;

    @Column()
    frequency: string;

    @Column({nullable: true})
    weekday: string;

    @OneToMany(type => Event, event => event.task)
    events: Event[]

    @ManyToOne(type => Category, category => category.tasks)
    category: Category;

    static async getTasksByDay(filter:FilterDto, limit:number, offset:number): Promise<[Task[], number]>{
        const qb = Task.createQueryBuilder('task');
        qb.leftJoinAndSelect("task.category", "category")
        .orderBy('task.title', 'ASC')
        .offset(offset)
        .limit(limit);
        
        if (filter.date) {
            const dayIndex = moment(new Date(filter.date)).format('dddd');
            if (dayIndex) {
                qb.where('task.weekday = :day', { day: dayIndex });
            }  
            qb.orWhere('task.frequency = :freq', {freq: "Daily"});
        }
        return qb.getManyAndCount();
    }
    
    static async createTask(createTaskDto: CreateTaskDto, category: Category): Promise<Task> {
        const { title, location, frequency, day } = createTaskDto;
        const task = new Task();
        task.id = uuid();        
        task.title = title;
        task.location = location;
        task.frequency = frequency;
        task.weekday = day;
        task.category = category;
        await task.save()
        return task;
    }

    static async updateTask(task: Task ,updateTaskDto: UpdateTaskDto, category?: Category): Promise<Task> {
        const { title, location, frequency, day } = updateTaskDto;      
        task.title = title;
        task.location = location;
        task.frequency = frequency;
        task.weekday = day;
        task.category = category
        await task.save();
        return task;
    }
}

