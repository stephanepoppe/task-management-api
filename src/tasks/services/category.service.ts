import { Injectable, Inject } from "@nestjs/common";
import { CategoryRepository } from '../repository/category.repository';
import { Category } from "../entities/category.entity";
import { CreateCategory } from '../dto/create-category.dto';

@Injectable()
export class CategoryService {
    constructor(
        @Inject('CATEGORY_REPOSITORY')
        private categoryRepository: CategoryRepository,
    ){}

    async createCategory(createEventDto: CreateCategory): Promise<Category> {
        const category:Category = await Category.createCategory(createEventDto);
        return category;
    }

    async getCategories(): Promise<Category[]> {
        return this.categoryRepository.find();
    }

    async getCategoryById(id: string): Promise<Category> {
        return this.categoryRepository.findOne({where: {'id': id}});
    }
}