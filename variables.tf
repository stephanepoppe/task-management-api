variable "do_token" {
  description = "DigitalOcean Api Token"
}

variable "region" {
  description = "DigitalOcean region"
  default = "ams3"
}
variable "droplet_image" {
  description = "DigitalOcean droplet image name"
  default = "ubuntu-20-04-x64"
}

variable "droplet_size" {
  description = "Droplet size for server"
  default = "s-1vcpu-1gb"
}

variable "pvt_key" {
  description = "Private key path of the ssh token"
}

# variable "ssh_public_key" {
#   description = "Local public ssh key"
#   default = "~/.ssh/id_rsa.pub"
# }

variable "api_token" {
  default = ""
}